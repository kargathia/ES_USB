#pragma once

#include <libusb-1.0/libusb.h>
#include "Const.h"

class Context;
class EventHandler;

class Device {
 public:
  Device(Context& context, int VID, int PID);
  virtual ~Device();

  void Claim();
  void Send(Const::ControllerAction action);
  void SendRumble(uint8_t big, uint8_t small);
  void Read(EventHandler& handler);

 private:
  void ReadByte2(uint8_t val, EventHandler& handler);
  void ReadByte3(uint8_t val, EventHandler& handler);
  void ReadByte4(uint8_t val, EventHandler& handler);
  void ReadByte5(uint8_t val, EventHandler& handler);
  void ReadByte6(int16_t val, EventHandler& handler);
  void ReadByte8(int16_t val, EventHandler& handler);
  void ReadByteA(int16_t val, EventHandler& handler);
  void ReadByteC(int16_t val, EventHandler& handler);

  int16_t Make16(uint8_t left, uint8_t right);
  bool CheckBit(int val, int pos);

public:
  libusb_device_handle* Get() { return myDevice; }
 
 private:
  libusb_device_handle* myDevice;
};