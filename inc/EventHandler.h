#pragma once

#include <libusb-1.0/libusb.h>

class Device;

class EventHandler {
 public:
  void OnPadUp(Device* device);
  void OnPadDown(Device* device);
  void OnPadRight(Device* device);
  void OnPadLeft(Device* device);
  void OnStartButton(Device* device);
  void OnBackButton(Device* device);
  void OnLeftStick(Device* device);
  void OnRightStick(Device* device);

  void OnLB(Device* device);
  void OnRB(Device* device);
  void OnLogoButton(Device* device);
  void OnButtonA(Device* device);
  void OnButtonB(Device* device);
  void OnButtonX(Device* device);
  void OnButtonY(Device* device);

  void OnLeftTrigger(unsigned int val, Device* device);
  void OnRightTrigger(unsigned int val, Device* device);

  void OnLeftStickX(int val, Device* device);
  void OnLeftStickY(int val, Device* device);

  void OnRightStickX(int val, Device* device);
  void OnRightStickY(int val, Device* device);
};