#pragma once

#define MESSAGE 0x01
#define IS 0x03

// Type
#define OUTPUT 0x01
#define INPUT 0x81

namespace Const {
enum ControllerAction {
  AllOff = 0x00,
  AllBlinking = 0x01,
  OneFlashesThenOn = 0x02,
  TwoFlashesThenOn = 0x03,
  ThreeFlashesThenOn = 0x04,
  FourFlashesThenOn = 0x05,
  OneOn = 0x06,
  TwoOn = 0x07,
  ThreeOn = 0x08,
  FourOn = 0x09,
  Rotating = 0x0A,
  Blinking = 0x0B,
  SlowBlinking = 0x0C,
  Alternating = 0x0D
};

enum SecondByte {
  PadUp = 0x00,
  PadDown = 0x01,
  PadLeft = 0x02,
  PadRight = 0x03,
  StartButton = 0x04,
  BackButton = 0x05,
  LeftStick = 0x06,
  RightStick = 0x07
};

enum ThirdByte {
  LB = 0x0,
  RB = 0x1,
  LogoButton = 0x2,
  ButtonA = 0x4,
  ButtonB = 0x5,
  ButtonX = 0x6,
  ButtonY = 0x7
};

};