#Compiler and Linker
CC          := arm-linux-g++

# TARGET should equal the name of the containing directory
TARGET      := Controller

# TARTYPE can be "exe" or "static". This will decide how this module is compiled/linked
TARTYPE		:= exe

#The Directories, Source, Includes, Objects, Binary and Resources
SRCDIR      := src
INCDIR      := inc
BUILDDIR    := obj
TARGETDIR   := bin
RESDIR      := res
SRCEXT      := cpp
DEPEXT      := d
OBJEXT      := o

# Compilation flags.
CFLAGS      := -c -Wall -std=c++11 -Werror -g

# Executable linking flags. These are only used for "exe" $(TARTYPE)
LIB         := -lusb-1.0

# Include directories. Used modules (aka everything in this project) should not be added here
INC         := -I$(INCDIR)
INCDEP      := -I$(INCDIR)

# Dependencies on other modules in this project. Will be used for Include directories and Link dependencies
# This name should equal $(TARGET) / containing directory for used module dependencies
MODDEP		:= 

#---------------------------------------------------------------------------------
#DO NOT EDIT BELOW THIS LINE
#---------------------------------------------------------------------------------
SOURCES     := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS     := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))
LINKED		:= $(foreach mod,$(MODDEP),../$(mod)/$(TARGETDIR)/$(mod).$(OBJEXT))
INC			+= $(foreach mod,$(MODDEP),-I../$(mod)/$(INCDIR))

#Default Make
all: resources $(TARTYPE)

#Remake
remake: cleaner all

#Copy Resources from Resources Directory to Target Directory
resources: directories
		@if [ -d $(RESDIR) ]; then cp $(RESDIR)/* $(TARGETDIR)/; fi

#Make the Directories
directories:
		@mkdir -p $(TARGETDIR)
		@mkdir -p $(BUILDDIR)

#Clean only Objects
clean:
		@$(RM) -rf $(BUILDDIR)

#Full Clean, Objects and Binaries
cleaner: clean
		@$(RM) -rf $(TARGETDIR)

#Pull in dependency info for *existing* .o files
-include $(OBJECTS:.$(OBJEXT)=.$(DEPEXT))

#Compile to executable
exe: $(OBJECTS)
		$(CC) -o $(TARGETDIR)/$(TARGET) $^ $(LINKED) $(LIB)

#Install to Raspberry
install: exe
	scp $(TARGETDIR)/$(TARGET) root@10.0.0.42:~

#Static library
static: $(OBJECTS)
		ld -r -o $(TARGETDIR)/$(TARGET).$(OBJEXT) $^

#Compile
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
		@mkdir -p $(dir $@)
		$(CC) $(CFLAGS) $(INC) -c -o $@ $<
		@$(CC) $(CFLAGS) $(INCDEP) -MM $(SRCDIR)/$*.$(SRCEXT) > $(BUILDDIR)/$*.$(DEPEXT)
		@cp -f $(BUILDDIR)/$*.$(DEPEXT) $(BUILDDIR)/$*.$(DEPEXT).tmp
		@sed -e 's|.*:|$(BUILDDIR)/$*.$(OBJEXT):|' < $(BUILDDIR)/$*.$(DEPEXT).tmp > $(BUILDDIR)/$*.$(DEPEXT)
		@sed -e 's/.*://' -e 's/\\$$//' < $(BUILDDIR)/$*.$(DEPEXT).tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(BUILDDIR)/$*.$(DEPEXT)
		@rm -f $(BUILDDIR)/$*.$(DEPEXT).tmp

#Non-File Targets
.PHONY: all remake clean cleaner resources