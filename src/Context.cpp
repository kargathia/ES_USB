#include <iostream>
#include "Context.h"

Context::Context() {
  int error_code = libusb_init(&myContext);  // initialize a library session

  if (error_code < 0) {
    throw "Init error";
  }

  // set verbosity level to 3, as suggested in the documentation
  libusb_set_debug(myContext, 3);
}

Context::~Context() {
  libusb_exit(myContext);  // close the session
}