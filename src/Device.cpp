#include <iostream>
#include "Context.h"
#include "Device.h"
#include <stdexcept>
#include "EventHandler.h"

Device::Device(Context& context, int VID, int PID) {
  // open device with known values for VID and PID
  myDevice = libusb_open_device_with_vid_pid(context.Get(), VID, PID);

  // find out if kernel driver is attached
  if (libusb_kernel_driver_active(myDevice, 0) == 1) {
    if (libusb_detach_kernel_driver(myDevice, 0) == 0) {
      std::cout << "Driver detached" << std::endl;
    }
  }

  Claim();
}

Device::~Device() {
  libusb_release_interface(myDevice, 0);
  if (libusb_kernel_driver_active(myDevice, 0)) {
    libusb_attach_kernel_driver(myDevice, 0);
  }
  libusb_close(myDevice);
}

void Device::Claim() {
  int error_code = 0;

  error_code = libusb_claim_interface(myDevice, 0);

  if (error_code < 0) {
    throw "Failed to claim device";
  }
}

void Device::Send(Const::ControllerAction action) {
  int error_code = 0;
  int transferred = 0;

  unsigned char data[] = {MESSAGE, IS, action};
  error_code = libusb_interrupt_transfer(myDevice, OUTPUT, data, sizeof(data),
                                         &transferred, 0);

  if (error_code < 0) {
    throw "Failed to send";
  }
}

void Device::SendRumble(uint8_t big, uint8_t small) {
  int error_code = 0;
  int transferred = 0;

  //std::cout << "big: " << (int)big << " small: " << (int)small << std::endl;

  uint8_t data[] = {0x00, 0x08, 0x00, big, small, 0x00, 0x00, 0x00};
  error_code = libusb_interrupt_transfer(myDevice, OUTPUT, data, sizeof(data),
                                         &transferred, 0);

  if (error_code < 0) {
    throw "Failed to send";
  }
}

void Device::Read(EventHandler& handler) {
  int error_code = 0;
  int transferred = 0;

  uint8_t data[20] = {};
  error_code =
      libusb_interrupt_transfer(myDevice, INPUT, data, 20, &transferred, 0);

  if (error_code < 0) {
    throw "Failed to send";
  }

  if (data[0] == 0) {
    ReadByte2((int)data[2], handler);
    ReadByte3((int)data[3], handler);
    ReadByte4((int)data[4], handler);
    ReadByte5((int)data[5], handler);
    ReadByte6(Make16((int)data[6], (int)data[7]), handler);
    ReadByte8(Make16((int)data[8], (int)data[9]), handler);
    ReadByteA(Make16((int)data[10], (int)data[11]), handler);
    ReadByteC(Make16((int)data[12], (int)data[13]), handler);
  }
}

int16_t Device::Make16(uint8_t left, uint8_t right) {
  int16_t output = left;
  output += right << 8;
  return output;
}

bool Device::CheckBit(int val, int pos) { return (val >> pos) & 1; }

void Device::ReadByte2(uint8_t val, EventHandler& handler) {
  for (unsigned int i = 0; i < 8; ++i) {
    if (CheckBit(val, i)) {
      switch (i) {
        case Const::PadUp: {
          handler.OnPadUp(this);
          break;
        }
        case Const::PadDown: {
          handler.OnPadDown(this);
          break;
        }
        case Const::PadLeft: {
          handler.OnPadLeft(this);
          break;
        }
        case Const::PadRight: {
          handler.OnPadRight(this);
          break;
        }
        case Const::StartButton: {
          handler.OnStartButton(this);
          break;
        }
        case Const::BackButton: {
          handler.OnBackButton(this);
          break;
        }
        case Const::LeftStick: {
          handler.OnLeftStick(this);
          break;
        }
        case Const::RightStick: {
          handler.OnRightStick(this);
          break;
        }
      }
    }
  }
}

void Device::ReadByte3(uint8_t val, EventHandler& handler) {
  for (int i = 0; i < 8; ++i) {
    if (CheckBit(val, i)) {
      switch (i) {
        case Const::LB: {
          handler.OnLB(this);
          break;
        }
        case Const::RB: {
          handler.OnRB(this);
          break;
        }
        case Const::LogoButton: {
          handler.OnLogoButton(this);
          break;
        }
        case Const::ButtonA: {
          handler.OnButtonA(this);
          break;
        }
        case Const::ButtonB: {
          handler.OnButtonB(this);
          break;
        }
        case Const::ButtonX: {
          handler.OnButtonX(this);
          break;
        }
        case Const::ButtonY: {
          handler.OnButtonY(this);
          break;
        }
      }
    }
  }
}

void Device::ReadByte4(uint8_t val, EventHandler& handler) {
  handler.OnLeftTrigger(val, this);
}

void Device::ReadByte5(uint8_t val, EventHandler& handler) {
  handler.OnRightTrigger(val, this);
}

void Device::ReadByte6(int16_t val, EventHandler& handler) {
  handler.OnLeftStickX(val, this);
}

void Device::ReadByte8(int16_t val, EventHandler& handler) {
  handler.OnLeftStickY(val, this);
}

void Device::ReadByteA(int16_t val, EventHandler& handler) {
  handler.OnRightStickX(val, this);
}

void Device::ReadByteC(int16_t val, EventHandler& handler) {
  handler.OnRightStickY(val, this);
}