#include "Device.h"
#include <iostream>
#include "EventHandler.h"

void EventHandler::OnPadUp(Device* device) {
  std::cout << "Pad Up" << std::endl;
  //device->Send(Const::OneOn);
}

void EventHandler::OnPadDown(Device* device) {
  std::cout << "Pad Down" << std::endl;
  //device->Send(Const::ThreeOn);
}

void EventHandler::OnPadRight(Device* device) {
  std::cout << "Pad Right" << std::endl;
  //device->Send(Const::TwoOn);
}

void EventHandler::OnPadLeft(Device* device) {
  std::cout << "Pad Left" << std::endl;
  //device->Send(Const::FourOn);
}

void EventHandler::OnStartButton(Device* device) {
  std::cout << "Start" << std::endl;
  device->Send(Const::Rotating);
}

void EventHandler::OnBackButton(Device* device) {
  std::cout << "Back" << std::endl;
  device->Send(Const::AllOff);
}

void EventHandler::OnLeftStick(Device* device) {
  std::cout << "Back" << std::endl;
}

void EventHandler::OnRightStick(Device* device) {
    std::cout << "Right Stick" << std::endl;
}

void EventHandler::OnLB(Device* device) {
    std::cout << "LB" << std::endl;
}
void EventHandler::OnRB(Device* device) {
    std::cout << "RB" << std::endl;
}

void EventHandler::OnLogoButton(Device* device) {
    std::cout << "XBox Logo" << std::endl;
    device->Send(Const::AllBlinking);
}

void EventHandler::OnButtonA(Device* device) {
    std::cout << "A" << std::endl;
    device->Send(Const::OneOn);
}

void EventHandler::OnButtonB(Device* device) {
    std::cout << "B" << std::endl;
    device->Send(Const::TwoOn);
}

void EventHandler::OnButtonX(Device* device) {
    std::cout << "X" << std::endl;
    device->Send(Const::ThreeOn);
}

void EventHandler::OnButtonY(Device* device) {
    std::cout << "Y" << std::endl;
    device->Send(Const::FourOn);
}

void EventHandler::OnLeftTrigger(unsigned int val, Device* device) {
  std::cout << " TLeft: " << val;
  device->SendRumble(0, val);
}

void EventHandler::OnRightTrigger(unsigned int val, Device* device) {
  std::cout << " TRight: " << val;
  device->SendRumble(val, 0);
}

void EventHandler::OnLeftStickX(int val, Device* device) {
  std::cout << " leftX: " << val;
}

void EventHandler::OnLeftStickY(int val, Device* device) {
  std::cout << " leftY: " << val;
}

void EventHandler::OnRightStickX(int val, Device* device) {
  std::cout << " rightX: " << val;
}

void EventHandler::OnRightStickY(int val, Device* device) {
  std::cout << " rightY: " << val << std::endl;
}