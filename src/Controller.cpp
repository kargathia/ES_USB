#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <libusb-1.0/libusb.h>

#include "Const.h"
#include "Context.h"
#include "Device.h"
#include "EventHandler.h"

namespace {
int VID = 0x45e;
int PID = 0x28e;
}

int main() {
  Context context = Context();
  Device device(context, VID, PID);

  EventHandler handler = EventHandler();
  while (true) {
    device.Read(handler);
  }

  return 0;
}